﻿
namespace Bryan.Macias
{
    public abstract class Aprobador
    {
        // se escribe la clase con su atributo
        protected Aprobador _siguiente;
        // se escribe la operacion y lo instanciamos
        public void AgregarSiguiente(Aprobador aprobador)
        {
            _siguiente = aprobador;
        }

        public abstract void Procesar(Compra a);

     
    }
}
﻿using System;

namespace Bryan.Macias
{
    //se crea la clase Director que hereda de Aprobador
    public class Director : Aprobador
    {
        //se sosbreescribe la operacion
        public override void Procesar(Compra a)
        {
            //al ser el ultimo eslabon de la cadena no es necesario la condicion ya que al momento de recibir la solicitud el director tendra q aceptarla
            if (a.Importe < 2000)

                Console.WriteLine(string.Format("Compra aprobada por el {0}", this.GetType().Name));
            else
                //la operacion siguiente procesar no derivara la solicitud ya que no hay mas eslabones en la cadena en este caso se puso por si se queria anadir otro
                _siguiente.Procesar(a);
        }
    }
}
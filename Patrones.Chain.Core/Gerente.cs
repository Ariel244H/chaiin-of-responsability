﻿using System;

namespace Bryan.Macias
{
    // se crea la clase Gerente que hereda de Aprobador
    public class Gerente : Aprobador
    {
        //se sobreescribe la operacion
        public override void Procesar(Compra a)
        {
            //se crea la condicion para calcular el valor del importe
            if (a.Importe <= 1000)
            {
                Console.WriteLine(string.Format("Compra aprobada por el {0}", this.GetType().Name));
            }
            else
            {
                //se deriva la solicitud al siguiente eslabon
                _siguiente.Procesar(a);
            }
        }
    }
}